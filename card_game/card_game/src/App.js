import React from 'react';
import './App.css';
import CardGame from './CardGame.js';
import { Route, BrowserRouter ,Switch} from 'react-router-dom';


class App extends React.Component {

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Switch> 
            <Route exact path='/cardgame' component={CardGame} />
          </Switch>

        </div>
      </BrowserRouter>
      
    );
  }
}

export default App;
