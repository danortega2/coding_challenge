import React from 'react'
import backCard from './playing-card-back.png'
import './App.css'
import './CardGame.css'
import { withRouter} from 'react-router-dom'

class CardGame extends React.Component {
    constructor() {
        super();
        this.state = {
          deckResults: {},
          cardResults: {},
          deckId: 0,
          deckOfCards : [],
          backCardImage: backCard,
          cardsSelected: [],
          cardsIndex: [],
          cardsRemoved: 0,
          deletedCard: { 
                          "code": "00",
                          "value": "",
                          "image": "",
                          "images": {
                              "png": "",
                              "svg": ""
                          },
                          "suit": ""
                        }
        }
      }


    //Doinng a GET request to create a shuffled deck and storing it inside the state
    fetchDeck = () => {
      return fetch(`https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1`)
      .then(res => res.json())
      .then(json => this.setState({ deckResults: json }))
    }
    
    //Doing a GET request to retrieve cards from shuffled deck and storing it inside the state
    drawFromDeck = () => {
      return fetch('https://deckofcardsapi.com/api/deck/' + this.state.deckResults.deck_id + '/draw/?count=52')
      .then(res => res.json())
      .then(json => this.setState({cardResults: json}))
    }

    //Grabbing the draw count data and storing it inside state array
    storeCards = () => {
      const {cardResults} = this.state
      let tempArr = [] 
      
      for (var i = 0; i < cardResults.cards.length; i++) {
        tempArr.push(cardResults.cards[i])
      }
      
      this.setState({deckOfCards: tempArr})
    }

    startGame = async () => {
      this.setState({deckOfCards:[]})
      this.setState({cardsSelected:[]})
      this.setState({cardsIndex:[]})
      this.setState({cardsRemoved:0})
      await this.fetchDeck()
      await this.drawFromDeck()
      await this.storeCards()

    }

    handleClick = async (e) => {
      const {deckOfCards, cardsSelected, cardsIndex, cardsRemoved, deletedCard, backCardImage} = this.state
    
      if(cardsSelected.length < 2) {
        e.target.src=e.target.getAttribute("alt")
        let index = e.target.getAttribute("id")
        cardsSelected.push(deckOfCards[index])
        cardsIndex.push(index)
       
        if(cardsSelected.length === 2) {
          
          const sleep = (milliseconds) => {
            return new Promise(resolve => setTimeout(resolve, milliseconds))
          }
          await sleep(1000)

           if(cardsSelected[0].value === cardsSelected[1].value && cardsIndex[0] !== cardsIndex[1]) {
             deckOfCards[cardsIndex[0]] =  deletedCard
             deckOfCards[cardsIndex[1]]  = deletedCard
             document.getElementById(cardsIndex[0]).src=""
             document.getElementById(cardsIndex[1]).src=""           
             this.setState({deckOfCards: deckOfCards})
             this.setState({cardsRemoved: cardsRemoved + 2})

           }
           else {
            document.getElementById(cardsIndex[0]).src=backCardImage
            document.getElementById(cardsIndex[1]).src=backCardImage
           }

          this.setState({cardsSelected:[]})
          this.setState({cardsIndex:[]})
        }

      }

    }

    makeTable = () => {
      const {deckOfCards, cardsRemoved, backCardImage} = this.state
  
      if(deckOfCards.length > 0 && cardsRemoved !== 52) {
        let rows = []
        let index = null
        const MAX_COL = 13

        for(var r = 0; r < 4; r++) {
          rows.push(<tr></tr>)
          for(var c = 0; c < 13; c++) {
            index = r*MAX_COL + c
            rows.push(<td>
                      <img className="tableData" key={index} id={index} src={backCardImage} alt={deckOfCards[index].image} onClick={this.handleClick}></img>
                      </td>)
          }
        }
      
        return rows
      }
      else if(cardsRemoved === 52) {
        return <td>Game Over</td>
      }
      
    }

    render() {
      return (
        <div className="CardGame">
          <button ref="newGameButton" className="NewGameButton" onClick={this.startGame}> 
            New Game
          </button>
          <table id="simple-board">
            <tbody>
              {this.makeTable()}
            </tbody>
          </table>
        </div>
      )
    }
}

export default withRouter(CardGame);