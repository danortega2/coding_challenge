package dao

import "coding-exercise/models"

type InventoryDao interface {
	GetInventoryItem(itemId string) (*models.InventoryItem, error)
}

//intentionally not implemented as unit test interactions with this interface should be mocked

func GetInventoryItem(itemId String)(*models.InventoryItem, error) {
	if(itemId == nil) {
		return nil, errors.New("Item does not exist")
	}
	else
		return &itemId, error
}
